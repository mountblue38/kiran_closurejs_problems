function limitFunctionCallCount(cbFunction, n) {
    if (
        typeof cbFunction != "function" ||
        cbFunction == undefined ||
        n == undefined
    ) {
        return [];
    }
    let count = 0;
    return function () {
        if (count != n) {
            count++;
            return cbFunction();
        } else {
            return null;
        }
    };
}

module.exports = limitFunctionCallCount;
