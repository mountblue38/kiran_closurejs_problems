function cacheFunction(cb) {
    let cacheObj = {};
    return function (cbInput) {
        if (cbInput in cacheObj) {
            return cacheObj[cbInput];
        }
        cacheObj[cbInput] = cb(cbInput);
        return cacheObj[cbInput];
    };
}

module.exports = cacheFunction;
