function counterFactory() {
    let counter = 0;
    function incrementCounter() {
        counter++;
        return counter;
    }
    function decrementCounter() {
        counter--;
        return counter;
    }
    return { incrementCounter, decrementCounter };
}

module.exports = counterFactory;
