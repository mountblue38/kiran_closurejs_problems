const counterFactory = require("../counterFactory.cjs");

let counter1 = counterFactory();
console.log(counter1.incrementCounter());
console.log(counter1.incrementCounter());
console.log(counter1.incrementCounter());
console.log(counter1.incrementCounter());
console.log(counter1.incrementCounter());
counter1.decrementCounter();
console.log(counter1.decrementCounter());
console.log(counter1.decrementCounter());

let counter2 = counterFactory();
console.log(counter2.incrementCounter());