const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

let functionLimiter = limitFunctionCallCount(function () {
    return "callable";
}, 2);
console.log(functionLimiter());
console.log(functionLimiter());
console.log(functionLimiter());
console.log(functionLimiter());
